package com.softtek.academiajavaweb.device;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;


public class WritterExcel {
	public void wExcel (List<Device> listDev,String path) throws FileNotFoundException, IOException {
		 Workbook workbook = new HSSFWorkbook();
		    org.apache.poi.ss.usermodel.Sheet sheet = workbook.createSheet();
		 
		    int rowCount = 0;
		 
		    for (Device aDevice : listDev) {
		        Row row = sheet.createRow(++rowCount);
		        writeBook(aDevice, row);
		    }
		 
		    try (FileOutputStream outputStream = new FileOutputStream(path)) {
		        workbook.write(outputStream);
		    }
	}
	
	private void writeBook(Device aDevice, Row row) {
	    Cell cell = row.createCell(1);
	    cell.setCellValue(aDevice.getId());
	 
	    cell = row.createCell(2);
	    cell.setCellValue(aDevice.getName());
	 
	    cell = row.createCell(3);
	    cell.setCellValue(aDevice.getDescription());
	    cell = row.createCell(4);
	    cell.setCellValue(aDevice.getManufatureId());
	    cell = row.createCell(5);
	    cell.setCellValue(aDevice.getColorId());
	    cell = row.createCell(6);
	    cell.setCellValue(aDevice.getCommets());
	}

}
