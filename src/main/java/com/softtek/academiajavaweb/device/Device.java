package com.softtek.academiajavaweb.device;



public class Device {
	int id ;
    String name;
    String description;
    int ManufatureId ;
    int colorId ;
    String commets ;
	
    public Device(int id, String name, String description, int manufatureId, int colorId, String commets) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		ManufatureId = manufatureId;
		this.colorId = colorId;
		this.commets = commets;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getManufatureId() {
		return ManufatureId;
	}
	public void setManufatureId(int manufatureId) {
		ManufatureId = manufatureId;
	}
	public int getColorId() {
		return colorId;
	}
	public void setColorId(int colorId) {
		this.colorId = colorId;
	}
	public String getCommets() {
		return commets;
	}
	public void setCommets(String commets) {
		this.commets = commets;
	}
	public Device getDev () {
		return this;
	}
	@Override
	public String toString() {
		return this.id + " " + this.name+ " " + this.description+" " +this.ManufatureId+" " + this.colorId +" "+ this.commets;
	}
	
	
    
    

}
