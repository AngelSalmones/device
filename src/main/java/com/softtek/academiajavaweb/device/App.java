package com.softtek.academiajavaweb.device;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws FileNotFoundException, IOException
    {
    	String SQL_SELECT = "Select * from Device";
    try {
      
       Connection conn = DbConnection.getConnection();
       PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);
       ResultSet resultSet = preparedStatement.executeQuery();
       
       List<Device> device = new ArrayList<>();
       while (resultSet.next()) {
       	
      	  int id = resultSet.getInt("DEVICEID");
          String name = resultSet.getString("NAME");
          String description = resultSet.getString("DESCRIPTION");
          int ManufatureId = resultSet.getInt("MANUFACTURERID");
          int colorId = resultSet.getInt("COLORID");
          String commets = resultSet.getString("COMMENTS");
       
          Device dev = new Device (id,name,description,ManufatureId,colorId,commets);
          device.add(dev);
          
      }
       
       List<String> nameDevice = device.stream()
       .map((device1) -> device1.getName())
       .collect(
    		   Collectors.toList());
       System.out.println(nameDevice);
       
       List<Device> deviceLap= device.stream()
       .filter(dev1->dev1.getName().equals("Laptop"))
       .collect(Collectors.toList());
       System.out.println(deviceLap);
       
       List<String> finispoitn = device.stream()
    		   .map(dev1 -> dev1.getName())
    		   .filter(dev1 -> dev1.equals("Laptop"))
    		   .collect(Collectors.toList());
       System.out.println(finispoitn);
       
       Long contador = device.stream()
       .filter((Dev1)-> Dev1.getManufatureId()==3 )
       .count()
       ;
    
       System.out.println("Numero de productos con manufacturer con id =3 es: "+ contador);
       List<Device> devs = device.stream()
    		   .filter(dev1 -> dev1.getColorId()==1)
    		   .collect(Collectors.toList())
    		   ;
       System.out.println(devs);
       Map<Integer, Device> devices = device.stream()
    		   .collect(Collectors.toMap(Device::getId, Device::getDev))
    				  ;
       System.out.println(devices);
       
       WritterExcel excelWriter = new WritterExcel();       
       excelWriter.wExcel(device, "Device.xls");
       
    }catch(SQLException e){
    	System.out.print(e);
    }
    }
}
